module gitlab.com/ponkey364/mpbf-glimesh

go 1.16

require (
	gitlab.com/ponkey364/glimwsc v0.2.1
	gitlab.com/ponkey364/mpbf v0.0.0-20210912112701-123012b2fc2d
)

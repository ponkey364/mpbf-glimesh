package mpbf_glimesh

import (
	"context"
	_ "embed"
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/ponkey364/glimwsc"
	"gitlab.com/ponkey364/mpbf"
)

const PlatformName = "glimesh"

//go:embed subscribeToMessage.gql
var joinMessageSub string

var (
	ErrInChannel = errors.New("already in channel")
	ErrNoChannel = errors.New("we are not subscribed to a channel with that ID")
)

type GlimeshPlatform struct {
	session      *glimwsc.Session
	channelCache map[string]struct{}
	isOpen       bool
	messageLoop  chan json.RawMessage
	stop         chan bool
}

type msg struct {
	ID         string
	Message    string
	InsertedAt string
	Channel    struct {
		ID       string
		Streamer struct {
			ID       string
			Username string
		}
	}
	User struct {
		ID       string
		Username string
	}
}

func (g *GlimeshPlatform) Open(mc *mpbf.BotCommander, onMessage mpbf.CommandHandler) error {
	err := g.session.Open()
	if err != nil {
		return err
	}

	g.messageLoop = make(chan json.RawMessage)
	g.stop = make(chan bool)

	g.isOpen = true

	for realmID := range g.channelCache {
		err := g.session.StartSubscription(joinMessageSub, map[string]interface{}{
			"channel": realmID,
		}, g.pipeToMPBF)
		if err != nil {
			fmt.Println(err)
		}
	}

	go func() {
		for {
			select {
			case m := <-g.messageLoop:
				{
					// 1. deserialise the message
					data := &struct {
						ChatMessage *msg
					}{}

					err := json.Unmarshal(m, data)
					if err != nil {
						fmt.Println("Error", err)
					}

					chatMessage := data.ChatMessage

					// 2. check if the channel is in the cache
					if _, ok := g.channelCache[chatMessage.Channel.ID]; !ok {
						// we're not in this channel?
						continue
					}

					// 3. do the regular mpbf message handling stuff
					msg := &mpbf.Message{
						Content: chatMessage.Message,
						Author: &mpbf.User{
							ID:   chatMessage.User.ID,
							Name: chatMessage.User.Username,
						},
						Realm: &mpbf.Realm{
							ID: chatMessage.Channel.ID,
						},
					}

					ctx := context.WithValue(context.Background(), "glimesh.message", chatMessage)
					ctx = context.WithValue(ctx, "platform", PlatformName)

					onMessage(&mpbf.Request{Message: msg, Context: ctx}, nil)
				}
			case <-g.stop:
				return
			}
		}
	}()

	return nil
}

func (g *GlimeshPlatform) Close() error {
	g.session.Close()
	return nil
}

func (g *GlimeshPlatform) GetPlatformType() string {
	return PlatformName
}

func (g *GlimeshPlatform) JoinRealm(realmID string) error {
	// is this channel already in the map?
	if _, ok := g.channelCache[realmID]; ok {
		// yes
		return ErrInChannel
	}

	g.channelCache[realmID] = struct{}{}

	// if the connection is open, start the sub
	if g.isOpen {
		return g.session.StartSubscription(joinMessageSub, map[string]interface{}{
			"channel": realmID,
		}, g.pipeToMPBF)
	}

	// if the connection is closed, we just pop it in the cache and presume we'll grab it when we connect / reconnect

	return nil
}

func (g *GlimeshPlatform) LeaveRealm(realmID string) error {
	// since GraphQL doesn't have a way of unsubscribing, and glimwsc doesn't provide a workaround (fix tba), we just yoink from the channelCache and the goroutine in Open just ignores the messages.
	if _, ok := g.channelCache[realmID]; !ok {
		// uh no?
		return ErrNoChannel
	}

	delete(g.channelCache, realmID)
	return nil
}

func (g *GlimeshPlatform) GetSession() interface{} {
	return g.session
}

func (g *GlimeshPlatform) GetArgParser() func(rawArgs []string, command *mpbf.Command) ([]interface{}, error) {
	return mpbf.DefaultArgParser
}

func (g *GlimeshPlatform) pipeToMPBF(_ *glimwsc.Session, m json.RawMessage) {
	g.messageLoop <- m
}

func New(token string) (mpbf.Platform, error) {
	session := glimwsc.New(token, glimwsc.AuthModeToken)

	return &GlimeshPlatform{
		session:      session,
		channelCache: make(map[string]struct{}),
		isOpen:       false,
	}, nil
}
